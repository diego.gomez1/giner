#!/bin/bash

#dumpFile=./backups/dump_$(date +%d-%m-%Y"_"%H_%M_%S).dump
dumpFile=./backups/dump_$(date +%d-%m-%Y"_"%H_%M_%S).dump.tar
touch $dumpFile
# docker-compose -f docker-compose.dev.yml exec database pg_dumpall -c -U giner_user > $dumpFile
# docker-compose -f docker-compose.dev.yml exec database pg_dump -U giner_user -Fc giner_database > $dumpFile
docker-compose -f docker-compose.dev.yml exec database pg_dump --no-owner -Ft -U giner_user giner_database > $dumpFile
# gzip $dumpFile
