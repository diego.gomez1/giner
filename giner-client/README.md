# GinerClient

# Generate sdk from api

Project is https://github.com/cyclosproject/ng-openapi-gen

First make sure the api is running in development mode.

Then you can regenerate api. Make sure to restart api server if you make a migration. Check [main project `README.md`](../README.md)

```bash
$ npm run gen-sdk
```
