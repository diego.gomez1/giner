import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { EditorModule } from 'primeng/editor';
import { ToastModule } from 'primeng/toast';
import { MessageService, TreeDragDropService } from 'primeng/api';
import { CardModule } from 'primeng/card';
import { TreeTableModule } from 'primeng/treetable';
import { TreeModule } from 'primeng/tree';
import { DragDropModule } from 'primeng/dragdrop';
import { BadgeModule } from 'primeng/badge';
import { InplaceModule } from 'primeng/inplace';
import { InputTextModule } from 'primeng/inputtext';
import { TagModule } from 'primeng/tag';
import { ChipModule } from 'primeng/chip';
import { ChipsModule } from 'primeng/chips';
import { TooltipModule } from 'primeng/tooltip';
// import { TableModule } from 'primeng/table';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { MenuItem } from 'primeng/api';

const loadModules = [
  AccordionModule,
  ButtonModule,
  EditorModule,
  ToastModule,
  CardModule,
  TreeModule,
  TreeTableModule,
  DragDropModule,
  BadgeModule,
  InplaceModule,
  ChipModule,
  ChipsModule,
  InputTextModule,
  TagModule,
  TooltipModule,
  TieredMenuModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...loadModules],
  exports: [...loadModules],
  providers: [MessageService, TreeDragDropService],
})
export class PrimeModule {}
