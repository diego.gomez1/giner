export interface CFDocument {
  uri: string;
  identifier: string;
  lastChangeDateTime: Date;
  officialSourceURL: string;
  creator: string;
  publisher: string;
  title: string;
  description: string;
  language: string;
  adoptionStatus: string;
  statusStartDate: string;
}

export interface CFItemTypeURI {
  title: string;
  identifier: string;
  uri: string;
}

export interface CFItem {
  uri: string;
  identifier: string;
  lastChangeDateTime: Date;
  humanCodingScheme: string;
  fullStatement: string;
  abbreviatedStatement: string;
  statusStartDate: string;
  CFItemType: string;
  CFItemTypeURI: CFItemTypeURI;
  educationLevel: string[];
  notes: string;
}

export interface OriginNodeURI {
  title: string;
  identifier: string;
  uri: string;
}

export interface DestinationNodeURI {
  title: string;
  identifier: string;
  uri: string;
}

type AssociationType =
  | 'isChildOf'
  | 'isPeerOf'
  | 'isPartOf'
  | 'exactMatchOf'
  | 'precedes'
  | 'isRelatedTo'
  | 'replacedBy'
  | 'exemplar'
  | 'hasSkillLevel';

export interface CFAssociationTreeNode {
  item: CFItem;
  order: number;
  parent: CFAssociationTreeNode | null;
  children?: CFAssociationTreeNode[];
}

export interface CFAssociationTree {
  root: CFAssociationTreeNode;
}

export interface CFAssociation {
  uri: string;
  identifier: string;
  lastChangeDateTime: Date;
  originNodeURI: OriginNodeURI;
  associationType: AssociationType;
  destinationNodeURI: DestinationNodeURI;
  sequenceNumber: number;
}

export interface CFItemType {
  uri: string;
  identifier: string;
  lastChangeDateTime: Date;
  title: string;
  typeCode: string;
  hierarchyCode: string;
}

export interface CFDefinitions {
  CFConcepts: any[];
  CFSubjects: any[];
  CFLicenses: any[];
  CFItemTypes: CFItemType[];
  CFAssociationGroupings: any[];
}

export interface CFPackage {
  CFDocument: CFDocument;
  CFItems: CFItem[];
  CFAssociations: CFAssociation[];
  CFDefinitions: CFDefinitions;
}
