import { CFAssociationTree, CFAssociationTreeNode } from './CFModels';

export class SearchableTree {
  static fromCFAssociationTrees(cftrees: CFAssociationTree[]): SearchableTree {
    let tree = new SearchableTree();

    cftrees.map((cftree) => {
      tree.root.addChild(
        SearchableTreeNode.fromCFAssociationTreeNode(cftree.root)
      );
    });

    return tree;
  }

  root: SearchableTreeNode = new SearchableTreeNode('', 'parent');

  updateSearchMatch(searchTerm: string) {
    this.root.updateSearchMatch(searchTerm.toLocaleLowerCase('es'));
  }

  updateUsedNodesByUris(caseRefs: string[]) {
    this.root.updateUsedNodesByUris(caseRefs);
  }
}

export class SearchableTreeNode {
  children: SearchableTreeNode[] = [];
  parent: SearchableTreeNode | null;
  _searchMatch = false;
  _searchTerm = '';
  used = false;

  static fromCFAssociationTreeNode(
    cftreeNode: CFAssociationTreeNode
  ): SearchableTreeNode {
    let cfItem = cftreeNode.item;
    let text = cfItem.abbreviatedStatement;
    if (!text) {
      text = cfItem.fullStatement;
    }
    let sTreeNode = new SearchableTreeNode(text, cfItem.CFItemType, cfItem.uri);

    cftreeNode.children.map((cfnode) => {
      sTreeNode.addChild(SearchableTreeNode.fromCFAssociationTreeNode(cfnode));
    });

    return sTreeNode;
  }

  constructor(public label = '', public type = '', public uri = null) {}

  addChild(node: SearchableTreeNode) {
    this.children.push(node);
    node.parent = this;
  }

  updateSearchMatch(searchTerm) {
    let match = false;
    this.children.map((child) => {
      child.updateSearchMatch(searchTerm);
    });

    if (this.label.toLocaleLowerCase('es').indexOf(searchTerm) >= 0) {
      match = true;
    }

    if (!match) {
      this.children.map((child) => {
        match = match || child._searchMatch;
      });
    }

    this._searchMatch = match;
    this._searchTerm = searchTerm;
  }

  visible(): boolean {
    if (this._searchTerm && this._searchTerm.length > 0) {
      return this._searchMatch;
    } else {
      return true;
    }
  }

  updateUsedNodesByUris(caseRefs: string[]) {
    this.used = caseRefs.includes(this.uri);
    this.children.map((child) => child.updateUsedNodesByUris(caseRefs));
  }
}
