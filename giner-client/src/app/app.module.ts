import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrimeModule } from './prime/prime.module';
import { ApiModule } from './ginersdk/api.module';
import { PlansListComponent } from './components/plans/plans-list/plans-list.component';
import { HttpClientModule } from '@angular/common/http';
import { PlanEditComponent } from './components/plans/plan-edit/plan-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlanWeeksComponent } from './components/weeks/plan-weeks/plan-weeks.component';
import { WeekEditComponent } from './components/weeks/week-edit/week-edit.component';
import { DayEditComponent } from './components/days/day-edit/day-edit.component';
import { DayTagComponent } from './components/days/day-tag/day-tag.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CaseListComponent } from './components/case/case-list/case-list.component';
import { environment } from '../environments/environment';
import { InsolatedComponent } from './test/insolated/insolated.component';
import { CaseEditViewComponent } from './components/days/day-edit/case-edit-view/case-edit-view.component';
import { TextEditViewComponent } from './components/days/day-edit/text-edit-view/text-edit-view.component';
import { CaseCardComponent } from './components/case/case-card/case-card.component';
import { CfstylePipe } from './components/case/cfstyle.pipe';
import { CfShortTextPipe } from './components/case/cf-short-text.pipe';
import { CfPathPipe } from './components/case/cf-path.pipe';
import { CaseFoldableNodeComponent } from './components/case/case-list/case-foldable-node/case-foldable-node.component';
import { SearchCaseComponent } from './components/case/search-case/search-case.component';
import { FoldableTreeNodeComponent } from './components/case/search-case/foldable-tree-node/foldable-tree-node.component';

@NgModule({
  declarations: [
    AppComponent,
    PlansListComponent,
    PlanEditComponent,
    PlanWeeksComponent,
    WeekEditComponent,
    DayEditComponent,
    DayTagComponent,
    PageNotFoundComponent,
    CaseListComponent,
    InsolatedComponent,
    CaseEditViewComponent,
    TextEditViewComponent,
    CaseCardComponent,
    CfstylePipe,
    CfShortTextPipe,
    CfPathPipe,
    CaseFoldableNodeComponent,
    SearchCaseComponent,
    FoldableTreeNodeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PrimeModule,
    ApiModule.forRoot({ rootUrl: environment.ginerApi.url }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
