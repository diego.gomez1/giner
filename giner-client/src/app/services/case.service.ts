import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  catchError,
  filter,
  map,
  mergeAll,
  retry,
  shareReplay,
  tap,
} from 'rxjs/operators';
import {
  CFAssociation,
  CFAssociationTree,
  CFAssociationTreeNode,
  CFItem,
  CFPackage,
} from '../models/CFModels';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { SearchableTree, SearchableTreeNode } from '../models/searchableTree';

@Injectable({
  providedIn: 'root',
})
export class CaseService {
  // 'https://case.escuelaideo.edu.es/ims/case/v1p0/CFPackages/b7642646-72a4-11eb-bc61-0242c0a88003'
  baseCasePackageUri = `${environment.case.url}/ims/case/v1p0/CFPackages/`;

  constructor(private http: HttpClient) {}

  private _packages$: { [key: string]: Observable<CFPackage> } = {};

  getPackage(documentId: string): Observable<CFPackage> {
    if (!this._packages$[documentId]) {
      this._packages$[documentId] = this.http
        .get<CFPackage>(`${this.baseCasePackageUri}${documentId}`)
        .pipe(shareReplay({ refCount: true, bufferSize: 1 }));
    }
    return this._packages$[documentId];
  }

  getCFTreeNodes(
    documentId: string,
    rootNodeUri: string = null
  ): Observable<{ [key: string]: CFAssociationTreeNode }> {
    return this.getPackage(documentId).pipe(
      filter((data) => data !== null),
      map((pack: CFPackage): [CFAssociation[], CFItem[]] => {
        return [pack.CFAssociations, pack.CFItems];
      }),
      map(([associations, items]): { [key: string]: CFAssociationTreeNode } => {
        let itemsByUri: { [key: string]: CFAssociationTreeNode } = {};
        items.map((item: CFItem) => {
          itemsByUri[item.uri] = {
            item,
            parent: null,
            children: [],
            order: 0,
          };
        });

        associations.map((association) => {
          let from = itemsByUri[association.originNodeURI.uri];
          let to = itemsByUri[association.destinationNodeURI.uri];

          if (from && to) {
            switch (association.associationType) {
              case 'isChildOf':
                to.children.push(from);
                from.parent = to;
                from.order = association.sequenceNumber;
                break;

              // TODO: Add rest of relations
            }
          }
        });

        return itemsByUri;
      })
    );
    // .subscribe(this.trees$);
  }

  getCFTree(
    documentId: string,
    rootNodeUri: string = null
  ): Observable<CFAssociationTree[]> {
    return this.getCFTreeNodes(documentId).pipe(
      map((itemsByUri) => {
        let trees: CFAssociationTree[] = [];
        if (rootNodeUri !== null) {
          if (itemsByUri[rootNodeUri]) {
            trees.push({ root: itemsByUri[rootNodeUri] });
          }
        } else {
          trees = Object.values(itemsByUri)
            .filter((item) => item.parent == null)
            .map(
              (root): CFAssociationTree => {
                return { root };
              }
            );
        }

        return trees;
      })
    );
  }

  getSimpleTree(
    documentId: string,
    rootNodeUri: string = null
  ): Observable<SearchableTree> {
    return this.getCFTree(documentId, rootNodeUri).pipe(
      map((cfTrees: CFAssociationTree[]) => {
        return SearchableTree.fromCFAssociationTrees(cfTrees);
      })
    );
  }

  getItemByUriAtDocument(
    uri: string,
    documentId: string
  ): Observable<CFAssociationTreeNode> {
    return this.getCFTreeNodes(documentId).pipe(
      map((nodes) => nodes[uri]),
      tap((data) => {
        console.log(`requested ${uri} got `, data);
      })
    );
  }
}
