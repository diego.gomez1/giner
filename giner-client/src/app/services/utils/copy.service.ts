import { Injectable } from '@angular/core';
import { DayPartial } from '../../ginersdk/models/day-partial';

@Injectable({
  providedIn: 'root',
})
export class CopyService {
  private _dayAtBin = null;

  constructor() {}

  setDayToBin(day: DayPartial) {
    this._dayAtBin = { ...day };
  }

  pasteDayFromBin(day: DayPartial): DayPartial {
    if (this._dayAtBin) {
      day.caseRefs = this._dayAtBin.caseRefs;
      day.description = this._dayAtBin.description;
      day.tags = this._dayAtBin.tags;
    }
    return day;
  }
}
