import { TestBed } from '@angular/core/testing';

import { DayRepositoryService } from './day-repository.service';

describe('DayRepositoryService', () => {
  let service: DayRepositoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DayRepositoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
