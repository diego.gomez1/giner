import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { DayControllerService } from '../ginersdk/services/day-controller.service';

@Injectable({
  providedIn: 'root',
})
export class PlanRepositoryService {
  constructor(private dayController: DayControllerService) {}

  async getUsedCaseRefsAtPlan(planId: number) {
    let elements = await this.dayController
      .find({
        filter: JSON.stringify({
          where: {
            and: [{ 'week.plan': planId }, { caseRefs: { neq: null } }],
          },
          fields: {
            caseRefs: true,
          },
        }),
      })
      .pipe(
        map((elements) => {
          return [].concat(...elements.map((item) => item.caseRefs));
        }),
        map((elements) => [...new Set(elements)])
      )
      .toPromise();

    return elements;
  }
}
