import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DayWithRelations } from '../ginersdk/models/day-with-relations';
import { DayControllerService } from '../ginersdk/services/day-controller.service';

@Injectable({
  providedIn: 'root',
})
export class DayRepositoryService {
  constructor(private dayController: DayControllerService) {}

  async addCaseRefToDay(
    dayId: number,
    caseRef: string
  ): Promise<DayWithRelations> {
    let day = await this.dayController.findById({ id: dayId }).toPromise();
    if (!day.caseRefs) {
      day.caseRefs = [];
    }
    day.caseRefs.push(caseRef);
    day.caseRefs = [...new Set(day.caseRefs)];
    await this.dayController
      .updateById({
        id: dayId,
        body: {
          caseRefs: day.caseRefs,
        },
      })
      .toPromise();
    return day;
  }
}
