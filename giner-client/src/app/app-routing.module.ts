import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaseListComponent } from './components/case/case-list/case-list.component';
import { PlanEditComponent } from './components/plans/plan-edit/plan-edit.component';
import { PlansListComponent } from './components/plans/plans-list/plans-list.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { InsolatedComponent } from './test/insolated/insolated.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/plans',
    pathMatch: 'full',
  },
  {
    path: 'plans',
    component: PlansListComponent,
  },
  {
    path: 'plans/:id/edit',
    component: PlanEditComponent,
  },
  {
    path: 'test',
    component: InsolatedComponent,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];
/*
 */

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
