/* tslint:disable */
/* eslint-disable */
import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationParams } from './api-configuration';

import { DayControllerService } from './services/day-controller.service';
import { DayWeekControllerService } from './services/day-week-controller.service';
import { PingControllerService } from './services/ping-controller.service';
import { PlanControllerService } from './services/plan-controller.service';
import { PlanWeekControllerService } from './services/plan-week-controller.service';
import { WeekControllerService } from './services/week-controller.service';
import { WeekDayControllerService } from './services/week-day-controller.service';
import { WeekPlanControllerService } from './services/week-plan-controller.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    DayControllerService,
    DayWeekControllerService,
    PingControllerService,
    PlanControllerService,
    PlanWeekControllerService,
    WeekControllerService,
    WeekDayControllerService,
    WeekPlanControllerService,
    ApiConfiguration
  ],
})
export class ApiModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor( 
    @Optional() @SkipSelf() parentModule: ApiModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
      'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
