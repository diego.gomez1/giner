/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Partial<Plan>, schemaOptions: { partial: true })
 */
export interface PlanPartial {
  caseDocumentId?: string;
  caseDocumentRootUri?: string;
  description?: string;
  id?: number;
  name?: string;
}
