/* tslint:disable */
/* eslint-disable */
import { DayWithRelations } from './day-with-relations';
import { PlanWithRelations } from './plan-with-relations';

/**
 * (tsType: WeekWithRelations, schemaOptions: { includeRelations: true })
 */
export interface WeekWithRelations {
  days?: Array<DayWithRelations>;
  description?: string;
  id?: number;
  order?: number;
  plan?: PlanWithRelations;
  planId?: number;
  startAt?: string;
  title?: string;
}
