/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Omit<Week, 'id'>, schemaOptions: { title: 'NewWeek', exclude: [ 'id' ] })
 */
export interface NewWeek {
  description?: string;
  order?: number;
  planId?: number;
  startAt?: string;
  title?: string;
}
