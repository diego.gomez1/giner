/* tslint:disable */
/* eslint-disable */
export interface Plan {
  caseDocumentId?: string;
  caseDocumentRootUri?: string;
  description?: string;
  id?: number;
  name: string;
}
