/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Omit<Day, 'id'>, schemaOptions: { title: 'NewDay', exclude: [ 'id' ] })
 */
export interface NewDay {
  caseRefs?: Array<string>;
  description?: string;
  name?: string;
  order?: number;
  tags?: Array<string>;
  weekId?: number;
}
