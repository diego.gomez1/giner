/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Partial<Week>, schemaOptions: { partial: true })
 */
export interface WeekPartial {
  description?: string;
  id?: number;
  order?: number;
  planId?: number;
  startAt?: string;
  title?: string;
}
