/* tslint:disable */
/* eslint-disable */
export interface Day {
  caseRefs?: Array<string>;
  description?: string;
  id?: number;
  name?: string;
  order?: number;
  tags?: Array<string>;
  weekId?: number;
}
