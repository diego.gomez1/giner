/* tslint:disable */
/* eslint-disable */
import { ScopeFilter as DayScopeFilter } from '../../Day/scope-filter';
export interface Items {
  relation?: string;
  scope?: DayScopeFilter;
}
