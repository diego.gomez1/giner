/* tslint:disable */
/* eslint-disable */
import { Items as DayIncludeFilterItems } from '../Day/IncludeFilter/items';
export interface Filter {
  fields?: { 'id'?: boolean, 'name'?: boolean, 'order'?: boolean, 'description'?: boolean, 'tags'?: boolean, 'caseRefs'?: boolean, 'weekId'?: boolean } | Array<'id' | 'name' | 'order' | 'description' | 'tags' | 'caseRefs' | 'weekId'>;
  include?: Array<DayIncludeFilterItems | string>;
  limit?: number;
  offset?: number;
  order?: string | Array<string>;
  skip?: number;
}
