/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: @loopback/repository-json-schema#Optional<Omit<Week, 'id'>, 'planId'>, schemaOptions: { title: 'NewWeekInPlan', exclude: [ 'id' ], optional: [ 'planId' ] })
 */
export interface NewWeekInPlan {
  description?: string;
  order?: number;
  planId?: number;
  startAt?: string;
  title?: string;
}
