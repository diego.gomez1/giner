/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: @loopback/repository-json-schema#Optional<Omit<Day, 'id'>, 'weekId'>, schemaOptions: { title: 'NewDayInWeek', exclude: [ 'id' ], optional: [ 'weekId' ] })
 */
export interface NewDayInWeek {
  caseRefs?: Array<string>;
  description?: string;
  name?: string;
  order?: number;
  tags?: Array<string>;
  weekId?: number;
}
