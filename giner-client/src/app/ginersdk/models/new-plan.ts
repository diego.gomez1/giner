/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Omit<Plan, 'id'>, schemaOptions: { title: 'NewPlan', exclude: [ 'id' ] })
 */
export interface NewPlan {
  caseDocumentId?: string;
  caseDocumentRootUri?: string;
  description?: string;
  name: string;
}
