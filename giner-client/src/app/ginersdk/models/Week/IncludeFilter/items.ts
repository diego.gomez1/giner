/* tslint:disable */
/* eslint-disable */
import { ScopeFilter as WeekScopeFilter } from '../../Week/scope-filter';
export interface Items {
  relation?: string;
  scope?: WeekScopeFilter;
}
