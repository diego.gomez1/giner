/* tslint:disable */
/* eslint-disable */
import { Items as WeekIncludeFilterItems } from '../Week/IncludeFilter/items';
export interface Filter {
  fields?: { 'id'?: boolean, 'title'?: boolean, 'description'?: boolean, 'order'?: boolean, 'startAt'?: boolean, 'planId'?: boolean } | Array<'id' | 'title' | 'description' | 'order' | 'startAt' | 'planId'>;
  include?: Array<WeekIncludeFilterItems | string>;
  limit?: number;
  offset?: number;
  order?: string | Array<string>;
  skip?: number;
}
