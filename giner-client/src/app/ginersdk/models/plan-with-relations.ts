/* tslint:disable */
/* eslint-disable */
import { WeekWithRelations } from './week-with-relations';

/**
 * (tsType: PlanWithRelations, schemaOptions: { includeRelations: true })
 */
export interface PlanWithRelations {
  caseDocumentId?: string;
  caseDocumentRootUri?: string;
  description?: string;
  id?: number;
  name: string;
  weeks?: Array<WeekWithRelations>;
}
