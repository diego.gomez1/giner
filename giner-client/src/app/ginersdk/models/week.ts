/* tslint:disable */
/* eslint-disable */
export interface Week {
  description?: string;
  id?: number;
  order?: number;
  planId?: number;
  startAt?: string;
  title?: string;
}
