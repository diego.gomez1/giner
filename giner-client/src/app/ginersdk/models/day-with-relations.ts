/* tslint:disable */
/* eslint-disable */
import { WeekWithRelations } from './week-with-relations';

/**
 * (tsType: DayWithRelations, schemaOptions: { includeRelations: true })
 */
export interface DayWithRelations {
  caseRefs?: Array<string>;
  description?: string;
  id?: number;
  name?: string;
  order?: number;
  tags?: Array<string>;
  week?: WeekWithRelations;
  weekId?: number;
}
