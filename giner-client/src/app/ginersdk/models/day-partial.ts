/* tslint:disable */
/* eslint-disable */

/**
 * (tsType: Partial<Day>, schemaOptions: { partial: true })
 */
export interface DayPartial {
  caseRefs?: Array<string>;
  description?: string;
  id?: number;
  name?: string;
  order?: number;
  tags?: Array<string>;
  weekId?: number;
}
