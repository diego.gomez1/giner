/* tslint:disable */
/* eslint-disable */
import { ScopeFilter as PlanScopeFilter } from '../../Plan/scope-filter';
export interface Items {
  relation?: string;
  scope?: PlanScopeFilter;
}
