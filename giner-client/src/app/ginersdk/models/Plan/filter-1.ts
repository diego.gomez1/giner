/* tslint:disable */
/* eslint-disable */
import { Items as PlanIncludeFilterItems } from '../Plan/IncludeFilter/items';
export interface Filter1 {
  fields?: { 'id'?: boolean, 'name'?: boolean, 'description'?: boolean, 'caseDocumentId'?: boolean, 'caseDocumentRootUri'?: boolean } | Array<'id' | 'name' | 'description' | 'caseDocumentId' | 'caseDocumentRootUri'>;
  include?: Array<PlanIncludeFilterItems | string>;
  limit?: number;
  offset?: number;
  order?: string | Array<string>;
  skip?: number;
  where?: { [key: string]: any };
}
