/* tslint:disable */
/* eslint-disable */
export interface ScopeFilter {
  fields?: { [key: string]: any } | Array<string>;
  include?: Array<{ [key: string]: any }>;
  limit?: number;
  offset?: number;
  order?: string | Array<string>;
  skip?: number;
  where?: { [key: string]: any };
}
