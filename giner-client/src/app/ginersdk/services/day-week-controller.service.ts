/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Week } from '../models/week';

@Injectable({
  providedIn: 'root',
})
export class DayWeekControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation dayWeekControllerGetWeek
   */
  static readonly DayWeekControllerGetWeekPath = '/days/{id}/week';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getWeek()` instead.
   *
   * This method doesn't expect any request body.
   */
  getWeek$Response(params: {
    id: number;
  }): Observable<StrictHttpResponse<Array<Week>>> {

    const rb = new RequestBuilder(this.rootUrl, DayWeekControllerService.DayWeekControllerGetWeekPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Week>>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getWeek$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getWeek(params: {
    id: number;
  }): Observable<Array<Week>> {

    return this.getWeek$Response(params).pipe(
      map((r: StrictHttpResponse<Array<Week>>) => r.body as Array<Week>)
    );
  }

}
