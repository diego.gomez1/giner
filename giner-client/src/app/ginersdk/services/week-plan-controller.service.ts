/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Plan } from '../models/plan';

@Injectable({
  providedIn: 'root',
})
export class WeekPlanControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation weekPlanControllerGetPlan
   */
  static readonly WeekPlanControllerGetPlanPath = '/weeks/{id}/plan';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getPlan()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlan$Response(params: {
    id: number;
  }): Observable<StrictHttpResponse<Array<Plan>>> {

    const rb = new RequestBuilder(this.rootUrl, WeekPlanControllerService.WeekPlanControllerGetPlanPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Plan>>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getPlan$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlan(params: {
    id: number;
  }): Observable<Array<Plan>> {

    return this.getPlan$Response(params).pipe(
      map((r: StrictHttpResponse<Array<Plan>>) => r.body as Array<Plan>)
    );
  }

}
