import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsolatedComponent } from './insolated.component';

describe('InsolatedComponent', () => {
  let component: InsolatedComponent;
  let fixture: ComponentFixture<InsolatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsolatedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsolatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
