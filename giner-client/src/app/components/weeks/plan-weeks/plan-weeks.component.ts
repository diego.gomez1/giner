import { Component, Input, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Plan, Week } from 'src/app/ginersdk/models';
import {
  PlanWeekControllerService,
  WeekDayControllerService,
} from 'src/app/ginersdk/services';

@Component({
  selector: 'app-plan-weeks',
  templateUrl: './plan-weeks.component.html',
  styleUrls: ['./plan-weeks.component.scss'],
})
export class PlanWeeksComponent implements OnInit {
  @Input('plan')
  plan: Plan;
  weeks: Week[];

  constructor(
    private planWeekController: PlanWeekControllerService,
    private weekDayController: WeekDayControllerService
  ) {}

  ngOnInit(): void {
    this.loadWeeks();
  }

  // http://angular.server:3000/plans/2/weeks?filter=%7B%0A%20%20%22include%22%3A%20%5B%22days%22%5D%0A%7D
  // http://angular.server:3000/plans/2/weeks?filter=%257B%250A%2520%2520%2522include%2522%253A%2520%255B%2522days%2522%255D%250A%257D

  loadWeeks() {
    this.planWeekController
      .find({
        id: this.plan.id,
        filter: JSON.stringify({
          include: ['days'],
          /* where: {
            title: 'Semana 2',
          }, */
        }),
      })
      // .find({ id: this.planId, filter: { filter: { include: ['days'] } } })
      .pipe(
        tap(console.log),
        map((data) => {
          return data.sort((a, b) => a.order - b.order);
        })
      )
      .subscribe((data) => {
        this.weeks = data;
      });
  }

  async generateWeeks() {
    for (let i = 0; i < 37; i++) {
      const weekTitle = `Semana ${i + 1}`;
      const generatedWeek = await this.planWeekController
        .create({
          id: this.plan.id,
          body: { title: weekTitle, order: i },
        })
        .toPromise();

      // Generate a day per week
      await Promise.all(
        ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'].map(
          async (day, index) => {
            const generatedDay = await this.weekDayController
              .create({
                id: generatedWeek.id,
                body: {
                  name: day,
                  order: index,
                  description: 'TBD',
                },
              })
              .toPromise();
            console.log(
              `Generated day ${generatedDay.id} at week ${generatedWeek.id}`,
              generatedDay
            );
          }
        )
      );
      console.log('Generated', generatedWeek);
    }
    this.loadWeeks();
  }
}
