import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanWeeksComponent } from './plan-weeks.component';

describe('PlanWeeksComponent', () => {
  let component: PlanWeeksComponent;
  let fixture: ComponentFixture<PlanWeeksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanWeeksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanWeeksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
