import { Component, Input, OnInit } from '@angular/core';
import {
  Day,
  DayWithRelations,
  Plan,
  Week,
  WeekWithRelations,
} from 'src/app/ginersdk/models';

@Component({
  selector: 'app-week-edit',
  templateUrl: './week-edit.component.html',
  styleUrls: ['./week-edit.component.scss'],
})
export class WeekEditComponent implements OnInit {
  @Input('week')
  week: WeekWithRelations;
  @Input('plan')
  plan: Plan;
  selectedDay: Day = null;

  constructor() {}

  ngOnInit(): void {}

  refreshDay(dayData: Day) {
    console.log('received event', dayData);
    this.week.days.map((day) => {
      if (day.id === dayData.id) {
        Object.assign(day, dayData);
      }
    });
  }

  toggleDay(day: Day) {
    if (this.selectedDay && this.selectedDay.id == day.id) {
      this.selectedDay = null;
    } else {
      this.selectedDay = day;
    }
  }
}
