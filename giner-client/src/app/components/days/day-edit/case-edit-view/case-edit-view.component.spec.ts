import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseEditViewComponent } from './case-edit-view.component';

describe('CaseEditViewComponent', () => {
  let component: CaseEditViewComponent;
  let fixture: ComponentFixture<CaseEditViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaseEditViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseEditViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
