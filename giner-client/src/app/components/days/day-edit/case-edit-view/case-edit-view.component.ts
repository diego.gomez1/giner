import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Day, DayWithRelations, Plan } from 'src/app/ginersdk/models';
import { DayControllerService } from 'src/app/ginersdk/services';
import { CFItem } from 'src/app/models/CFModels';
import { CaseService } from 'src/app/services/case.service';

@Component({
  selector: 'app-case-edit-view',
  templateUrl: './case-edit-view.component.html',
  styleUrls: ['./case-edit-view.component.scss'],
})
export class CaseEditViewComponent implements OnInit {
  dragging: boolean = false;
  draggedItem: string;
  private _day: DayWithRelations;
  @Input('plan')
  plan: Plan;
  @Input('day')
  get day(): DayWithRelations {
    return this._day;
  }
  set day(day: DayWithRelations) {
    this._day = day;
  }
  @Output() onUpdated: EventEmitter<Day> = new EventEmitter();

  constructor(
    private caseService: CaseService,
    private dayController: DayControllerService
  ) {}

  ngOnInit(): void {}

  startDragging(item) {
    this.draggedItem = item;
    this.dragging = true;
  }

  stopDragging() {
    this.draggedItem = null;
    this.dragging = false;
  }

  async dropToTrash() {
    if (this.draggedItem) {
      let dragRef = this.draggedItem;
      console.log('Remove', dragRef);
      this.day.caseRefs = this.day.caseRefs.filter((ref) => ref !== dragRef);

      await this.dayController
        .updateById({
          id: this.day.id,
          body: { caseRefs: this.day.caseRefs },
        })
        .toPromise();

      this.draggedItem = null;
      this.dragging = false;
      this.onUpdated.emit(this.day);
    }
  }
}
