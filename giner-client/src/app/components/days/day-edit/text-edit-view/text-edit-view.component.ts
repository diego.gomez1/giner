import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Day } from 'src/app/ginersdk/models';
import { DayControllerService } from 'src/app/ginersdk/services';

@Component({
  selector: 'app-text-edit-view',
  templateUrl: './text-edit-view.component.html',
  styleUrls: ['./text-edit-view.component.scss'],
})
export class TextEditViewComponent implements OnInit {
  private _day: Day;
  @Input('day')
  get day(): Day {
    return this._day;
  }
  set day(day: Day) {
    this._day = day;
    this._day.tags = this._day.tags || [];
    this.dayForm.patchValue(this._day);
  }
  @Output() onUpdated: EventEmitter<Day> = new EventEmitter();

  dayForm = this.fb.group({
    id: [null, Validators.required],
    description: [],
    tags: [[]],
    // caseRefs: [[]],
  });

  constructor(
    private fb: FormBuilder,
    private dayController: DayControllerService
  ) {}

  ngOnInit(): void {}

  saveDay() {
    if (this.dayForm.valid && this.dayForm.touched) {
      let updatedData = this.dayForm.getRawValue();

      this.dayController.updateById({ id: this.day.id, body: {} });
      this.dayController
        .updateById({
          id: this.day.id,
          body: updatedData,
        })
        .toPromise();

      this.dayForm.reset();
      this.dayForm.patchValue(updatedData);
      this.onUpdated.emit({ ...this.day, ...updatedData });
    }
  }

  // @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if ((event.metaKey || event.ctrlKey) && event.key === 's') {
      // this.save();
      console.log('Saving on day');
      this.saveDay();
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  }
}
