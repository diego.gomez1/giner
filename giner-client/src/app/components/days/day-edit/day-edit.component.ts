import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Day, Plan } from 'src/app/ginersdk/models';
import { DayControllerService } from 'src/app/ginersdk/services';
import { CopyService } from 'src/app/services/utils/copy.service';

@Component({
  selector: 'app-day-edit',
  templateUrl: './day-edit.component.html',
  styleUrls: ['./day-edit.component.scss'],
})
export class DayEditComponent implements OnInit {
  @Input('day')
  day: Day;
  @Input('plan')
  plan: Plan;
  @Output() onUpdated: EventEmitter<Day> = new EventEmitter();

  activeView: 'text' | 'case' = 'text';

  constructor(
    private copyService: CopyService,
    private dayController: DayControllerService
  ) {}

  ngOnInit(): void {}

  viewIsNot(value: string) {
    return this.activeView !== value;
  }

  dayUpdated(day: Day) {
    this.day = day;
    this.onUpdated.emit(day);
  }

  copyDay() {
    this.copyService.setDayToBin(this.day);
  }

  async pasteDay() {
    this.copyService.pasteDayFromBin(this.day);

    await this.dayController
      .updateById({
        id: this.day.id,
        body: {
          caseRefs: this.day.caseRefs || [],
          description: this.day.description,
          tags: this.day.tags || [],
        },
      })
      .toPromise();

    this.onUpdated.emit(this.day);
    this.day = this.day;
  }
}
