import { Component, Input, OnInit } from '@angular/core';
import { Day } from 'src/app/ginersdk/models';
import { CFAssociationTreeNode } from 'src/app/models/CFModels';
import { DraggingService } from 'src/app/services/utils/dragging.service';
import { DayRepositoryService } from 'src/app/services/day-repository.service';
import { CaseService } from 'src/app/services/case.service';

@Component({
  selector: 'app-day-tag',
  templateUrl: './day-tag.component.html',
  styleUrls: ['./day-tag.component.scss'],
})
export class DayTagComponent implements OnInit {
  @Input('day')
  day: Day;

  constructor(
    private caseService: CaseService,
    private drag: DraggingService,
    private dayRepository: DayRepositoryService
  ) {}

  ngOnInit(): void {}

  dragEnter() {
    console.log('In');
  }

  dragLeave() {
    console.log('Out');
  }

  async drop(event) {
    let node: CFAssociationTreeNode = this.drag.movingItem;
    if (node.item.uri) {
      console.log(`Asociar ${node.item.uri} a ${this.day.name}`);
      let updatedDay = await this.dayRepository.addCaseRefToDay(
        this.day.id,
        node.item.uri
      );
      this.day = updatedDay;
      console.log('Current day is', this.day);
    }
  }
}
