import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayTagComponent } from './day-tag.component';

describe('DayTagComponent', () => {
  let component: DayTagComponent;
  let fixture: ComponentFixture<DayTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayTagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
