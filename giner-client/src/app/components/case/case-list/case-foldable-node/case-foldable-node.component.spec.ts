import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseFoldableNodeComponent } from './case-foldable-node.component';

describe('CaseFoldableNodeComponent', () => {
  let component: CaseFoldableNodeComponent;
  let fixture: ComponentFixture<CaseFoldableNodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaseFoldableNodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseFoldableNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
