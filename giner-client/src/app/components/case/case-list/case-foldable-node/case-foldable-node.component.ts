import {
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { CFAssociationTreeNode, CFItem } from 'src/app/models/CFModels';

@Component({
  selector: 'app-case-foldable-node',
  templateUrl: './case-foldable-node.component.html',
  styleUrls: ['./case-foldable-node.component.scss'],
})
export class CaseFoldableNodeComponent implements OnInit {
  @Input()
  searchTerm: string;
  @Input()
  caseNode: CFAssociationTreeNode;
  @Input()
  folded = false;
  @ViewChildren(CaseFoldableNodeComponent)
  childNodes!: QueryList<CaseFoldableNodeComponent>;

  constructor() {}

  ngOnInit(): void {}

  getStyle(item: CFItem) {
    switch (item.CFItemType) {
      case 'Módulo':
        return 'cfmodule';
      case 'Contenido':
        return 'cfcontent';
      case 'Bloque':
        return 'cfblock';
      default:
        return `-${item.CFItemType}-`;
    }
  }

  toggleFold() {
    this.folded = !this.folded;
  }

  isNodeVisible() {
    let visible = this.displayNode();
    if (!visible) {
      this.childNodes.map((node) => {
        if (node.isNodeVisible()) {
          visible = true;
        }
      });
    }
    return visible;
  }

  displayNode() {
    if (this.searchTerm) {
      if (!this.caseNode.children || this.caseNode.children.length === 0) {
        let item = this.caseNode.item;
        let plainText = `${item.fullStatement} ${item.abbreviatedStatement}`;
        let containsText = plainText.indexOf(this.searchTerm) >= 0;

        return containsText;
      }
    } else {
      return true;
    }

    return false;
  }
}
