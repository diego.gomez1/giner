import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { TreeNode } from 'primeng/api';
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  of,
  Subscription,
} from 'rxjs';
import { debounceTime, filter, map, startWith, tap } from 'rxjs/operators';
import {
  CFAssociationTree,
  CFAssociationTreeNode,
  CFItem,
} from 'src/app/models/CFModels';
import { CaseService } from 'src/app/services/case.service';
import { PlanRepositoryService } from 'src/app/services/plan-repository.service';
import { DraggingService } from 'src/app/services/utils/dragging.service';
import {
  SearchableTree,
  SearchableTreeNode,
} from 'src/app/models/searchableTree';

const traverseTree = (node: CFAssociationTreeNode): CFAssociationTreeNode[] => {
  let flattenChildren = node.children.reduce((arrayItems, childNode) => {
    arrayItems.push(childNode);
    if (childNode.children.length > 0) {
      arrayItems = arrayItems.concat(traverseTree(childNode));
    }
    return arrayItems;
  }, []);

  return flattenChildren;
};

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.scss'],
})
export class CaseListComponent implements OnInit, OnDestroy {
  searchableTree$: Observable<SearchableTree>;
  @Input()
  caseDocumentId: string;

  @Input()
  rootItemUri: string;

  @Input()
  planId: number;

  caseTrees$ = new BehaviorSubject<CFAssociationTree[]>(null);
  caseItems$ = new BehaviorSubject<CFAssociationTreeNode[]>(null);
  usedItems: Set<string>;

  private _caseTreeSubscrition$: Subscription;
  private _latestSubscrition$: Subscription;

  filterSearch: FormControl = this.fb.control('');
  searchTerm$ = new BehaviorSubject<string>('');

  constructor(
    private caseService: CaseService,
    public drag: DraggingService,
    public fb: FormBuilder,
    private planRepository: PlanRepositoryService
  ) {}

  ngOnInit(): void {
    // this.caseTrees$ = new BehaviorSubject<CFAssociationTree[]>(null);
    // this.caseItems$ = new BehaviorSubject<CFAssociationTreeNode[]>(null);

    this.searchableTree$ = this.caseService.getSimpleTree(
      this.caseDocumentId,
      this.rootItemUri
    );

    this.filterSearch.valueChanges
      .pipe(debounceTime(400))
      .subscribe(this.searchTerm$);

    this.planRepository.getUsedCaseRefsAtPlan(this.planId).then((used) => {
      console.log(used);
      this.usedItems = new Set(used);
    });

    /*
    this._caseTreeSubscrition$ = this.caseService
      .getCFTree(this.caseDocumentId, this.rootItemUri)
      .subscribe(this.caseTrees$);

    this._latestSubscrition$ = combineLatest([
      this.caseTrees$.pipe(
        filter((caseTreeArr) => caseTreeArr !== null),
        map((caseTreeArr) => {
          return caseTreeArr
            .reduce((nodesArr, caseTree) => traverseTree(caseTree.root), [])
            .filter((node) => node.item);
        })
      ),
      this.filterSearch.valueChanges.pipe(startWith('')),
    ])
      .pipe(
        // debounceTime(500),
        tap(console.log),
        map(([tree, search]) => {
          if (search) {
            search = search.toLocaleLowerCase('es');
            return tree.filter((node: CFAssociationTreeNode) => {
              return (
                `${node.item.abbreviatedStatement} ${node.item.fullStatement}`
                  .toLocaleLowerCase('es')
                  .indexOf(search) >= 0
              );
            });
          } else {
            return tree;
          }
        })
      )
      .subscribe(this.caseItems$);
      */
  }

  ngOnDestroy() {
    /*
    this._caseTreeSubscrition$.unsubscribe();
    this._latestSubscrition$.unsubscribe();
    this.caseTrees$.next(null); */
  }

  dragStart(event, node) {
    console.log('Node', { event, node });
    this.drag.movingItem = node;
    return true;
  }

  dragEnd(event) {
    console.log('End', { event });
  }

  onNodeDrop(event) {
    console.log(event);
  }

  isUsed(item: CFItem) {
    return this.usedItems.has(item.uri);
  }

  getStyle(item: CFItem) {
    switch (item.CFItemType) {
      case 'Módulo':
        return 'cfmodule';
      case 'Contenido':
        return 'cfcontent';
      case 'Bloque':
        return 'cfblock';
      default:
        return `-${item.CFItemType}-`;
    }
  }
}
