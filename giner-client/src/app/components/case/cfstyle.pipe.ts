import { Pipe, PipeTransform } from '@angular/core';
import { CFItem } from 'src/app/models/CFModels';

@Pipe({
  name: 'cfstyle',
})
export class CfstylePipe implements PipeTransform {
  transform(item: CFItem, ...args: unknown[]): unknown {
    switch (item.CFItemType) {
      case 'Módulo':
        return 'cfmodule';
      case 'Contenido':
        return 'cfcontent';
      case 'Bloque':
        return 'cfblock';
      default:
        return `-${item.CFItemType}-`;
    }
  }
}
