import { Component, Input, OnInit } from '@angular/core';
import {
  SearchableTree,
  SearchableTreeNode,
} from 'src/app/models/searchableTree';
import { DraggingService } from 'src/app/services/utils/dragging.service';

@Component({
  selector: 'app-foldable-tree-node',
  templateUrl: './foldable-tree-node.component.html',
  styleUrls: ['./foldable-tree-node.component.scss'],
})
export class FoldableTreeNodeComponent implements OnInit {
  @Input()
  treeNode: SearchableTreeNode;
  @Input()
  folded = false;

  constructor(public drag: DraggingService) {}

  ngOnInit(): void {}

  getStyle(itemType: string) {
    switch (itemType) {
      case 'Módulo':
        return 'cfmodule';
      case 'Contenido':
        return 'cfcontent';
      case 'Bloque':
        return 'cfblock';
      default:
        return `-${itemType}-`;
    }
  }

  toggleFold() {
    this.folded = !this.folded;
  }

  dragStart(node: SearchableTreeNode) {
    this.drag.movingItem = {
      item: {
        uri: node.uri,
      },
    };
    return true;
  }

  dragEnd(event) {
    console.log('End', { event });
  }

  onNodeDrop(event) {
    console.log(event);
  }
}
