import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoldableTreeNodeComponent } from './foldable-tree-node.component';

describe('FoldableTreeNodeComponent', () => {
  let component: FoldableTreeNodeComponent;
  let fixture: ComponentFixture<FoldableTreeNodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoldableTreeNodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoldableTreeNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
