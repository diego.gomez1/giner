import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { debounceTime, filter, map, tap } from 'rxjs/operators';
import { SearchableTree } from 'src/app/models/searchableTree';
import { CaseService } from 'src/app/services/case.service';
import { DraggingService } from 'src/app/services/utils/dragging.service';
import { PlanRepositoryService } from 'src/app/services/plan-repository.service';

@Component({
  selector: 'app-search-case',
  templateUrl: './search-case.component.html',
  styleUrls: ['./search-case.component.scss'],
})
export class SearchCaseComponent implements OnInit, OnDestroy {
  _searchableTreeSub: Subscription;
  searchableTree$ = new BehaviorSubject<SearchableTree>(null);
  @Input()
  caseDocumentId: string;
  @Input()
  rootItemUri: string;
  @Input()
  planId: number;

  usedItems$ = new BehaviorSubject<Array<string>>([]);

  _filterSearchSub: Subscription;
  filterSearch: FormControl = this.fb.control('');
  _searchTermSub: Subscription;
  searchTerm$ = new BehaviorSubject<string>('');

  constructor(
    private caseService: CaseService,
    public drag: DraggingService,
    public fb: FormBuilder,
    private planRepository: PlanRepositoryService
  ) {}

  ngOnInit(): void {
    this.searchTerm$.next('');
    this.searchableTree$.next(null);
    console.log('create element');
    this._searchableTreeSub = this.caseService
      .getSimpleTree(this.caseDocumentId, this.rootItemUri)
      .subscribe(this.searchableTree$);

    this._filterSearchSub = this.filterSearch.valueChanges
      .pipe(debounceTime(400))
      .subscribe(this.searchTerm$);

    this._searchTermSub = this.searchTerm$.subscribe((term) => {
      let tree = this.searchableTree$.getValue();
      if (tree) {
        console.log(tree);
        console.log('Searching ', term);
        tree.updateSearchMatch(term);
      }
    });

    this.planRepository.getUsedCaseRefsAtPlan(this.planId).then((used) => {
      console.log(used);
      this.usedItems$.next([...new Set(used)]);
    });

    combineLatest([this.usedItems$, this.searchableTree$])
      .pipe(
        filter(([usedRefs, searchableTree]) => {
          return usedRefs !== null && searchableTree !== null;
        }),
        tap(([usedRefs, searchableTree]) =>
          console.log('updating', usedRefs, searchableTree)
        ),
        map(([usedRefs, searchableTree]) => {
          searchableTree.updateUsedNodesByUris(usedRefs);
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._searchableTreeSub?.unsubscribe();
    this._filterSearchSub?.unsubscribe();
    this._searchTermSub?.unsubscribe();
  }
}
