import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CFAssociationTreeNode, CFItem } from 'src/app/models/CFModels';
import { CaseService } from 'src/app/services/case.service';

@Component({
  selector: 'app-case-card',
  templateUrl: './case-card.component.html',
  styleUrls: ['./case-card.component.scss'],
})
export class CaseCardComponent implements OnInit {
  @Input('caseRef')
  caseRef: string;
  @Input('documentId')
  documentId: string;
  node$: Observable<CFAssociationTreeNode>;

  constructor(private caseService: CaseService) {}

  ngOnInit(): void {
    console.log('Display', { case: this.caseRef, doc: this.documentId });
    this.node$ = this.caseService.getItemByUriAtDocument(
      this.caseRef,
      this.documentId
    );
  }

  cardType(node: CFItem) {
    switch (node.CFItemType) {
      case 'Estándar':
        return 'standard';
      case 'Contenido':
        return 'content';
      case 'Bloque':
        return 'block';
      default:
        return 'undefined';
    }
  }
}
