import { Pipe, PipeTransform } from '@angular/core';
import { CFItem } from 'src/app/models/CFModels';

@Pipe({
  name: 'cfShortText',
})
export class CfShortTextPipe implements PipeTransform {
  transform(item: CFItem, ...args: unknown[]): unknown {
    let text = item.abbreviatedStatement;

    if (!text) {
      text = item.fullStatement;
    }

    return text;
  }
}
