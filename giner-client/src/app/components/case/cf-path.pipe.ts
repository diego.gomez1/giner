import { Pipe, PipeTransform } from '@angular/core';
import { CFAssociationTreeNode, CFItem } from '../../models/CFModels'; // 'src/app/models/CFModels';

@Pipe({
  name: 'cfPath',
})
export class CfPathPipe implements PipeTransform {
  transform(node: CFAssociationTreeNode, ...args: unknown[]): unknown {
    let nodes: CFItem[] = [];
    // nodes.push(node.item);
    let currentNode = node;

    while (currentNode.parent) {
      currentNode = currentNode.parent;
      nodes.push(currentNode.item);
    }

    let nodeTexts = [];

    while (nodes.length > 0) {
      let lastNode = nodes.pop();
      let text = lastNode.abbreviatedStatement;
      if (!text) {
        text = lastNode.fullStatement;
      }

      if (['Bloque', 'Módulo'].includes(lastNode.CFItemType))
        nodeTexts.push(text);
    }

    return nodeTexts.join('>');
  }
}
