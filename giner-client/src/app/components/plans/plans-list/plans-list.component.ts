import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Plan } from 'src/app/ginersdk/models/plan';
import { PlanControllerService } from 'src/app/ginersdk/services/plan-controller.service';

@Component({
  selector: 'app-plans-list',
  templateUrl: './plans-list.component.html',
  styleUrls: ['./plans-list.component.scss'],
})
export class PlansListComponent implements OnInit {
  plans$: Observable<Plan[]>;
  constructor(private planList: PlanControllerService) {}

  ngOnInit(): void {
    this.plans$ = this.planList.find({});
  }
}
