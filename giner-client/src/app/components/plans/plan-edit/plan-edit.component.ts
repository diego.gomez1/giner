import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter, map, mergeAll, tap } from 'rxjs/operators';
import { Plan } from 'src/app/ginersdk/models/plan';
import { PlanControllerService } from 'src/app/ginersdk/services/plan-controller.service';
import { PlanWeekControllerService } from 'src/app/ginersdk/services/plan-week-controller.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { PlanWithRelations } from 'src/app/ginersdk/models';

@Component({
  selector: 'app-plan-edit',
  templateUrl: './plan-edit.component.html',
  styleUrls: ['./plan-edit.component.scss'],
})
export class PlanEditComponent implements OnInit {
  activeId$ = new BehaviorSubject<number>(null);
  activePlan: PlanWithRelations = null;
  usedRefItems = [];
  planForm = this.fb.group({
    id: [null],
    name: ['', Validators.required],
    description: [''],
  });
  displayCase = false;

  constructor(
    private planController: PlanControllerService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {
    this.activeRoute.params
      .pipe(
        filter((params) => params !== null && Object.keys(params).length > 0),
        map((params) => params['id']),
        filter((id) => id !== null)
      )
      .subscribe(this.activeId$);

    this.activeId$
      .pipe(
        filter((id) => id !== null),
        map((planId) => {
          return this.planController.findById({
            id: planId,
            /* filter: JSON.stringify({
                include: [{ relation: 'weeks' }],
              }), */
          });
        }),
        mergeAll()
      )
      .subscribe((plan) => {
        this.activePlan = plan;
        this.planForm.patchValue(plan);
      });
  }

  ngOnInit(): void {}

  async save() {
    console.log('Saving document');
    if (this.planForm.touched) {
      console.log('Guardando cambios');
      const updatedPlan = this.planForm.getRawValue();
      Object.assign(this.activePlan, updatedPlan);
      let result = await this.planController
        .updateById({
          id: updatedPlan.id,
          body: updatedPlan,
        })
        .toPromise();
      this.messageService.add({
        severity: 'success',
        summary: 'Guardado',
        detail: 'Registro guardado correctamente',
      });
      this.planForm.reset();
      this.planForm.setValue(updatedPlan);

      console.log('Guardado', result);
    } else {
      console.log('No hay cambios pendientes');
    }
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if ((event.metaKey || event.ctrlKey) && event.key === 's') {
      this.save();
      event.preventDefault();
    }
  }
}
