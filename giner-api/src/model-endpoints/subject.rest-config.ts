import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Subject} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Subject,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/subjects',
};
module.exports = config;
