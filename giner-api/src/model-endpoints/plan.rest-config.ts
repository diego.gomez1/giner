import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Plan} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Plan,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/plans',
};
module.exports = config;
