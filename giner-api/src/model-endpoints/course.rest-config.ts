import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Course} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Course,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/courses',
};
module.exports = config;
