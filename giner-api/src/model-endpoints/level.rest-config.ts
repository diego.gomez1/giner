import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Level} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Level,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/levels',
};
module.exports = config;
