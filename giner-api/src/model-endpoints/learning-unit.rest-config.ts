import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LearningUnit} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LearningUnit,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/learning-units',
};
module.exports = config;
