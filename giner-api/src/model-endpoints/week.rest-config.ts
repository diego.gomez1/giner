import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Week} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Week,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/weeks',
};
module.exports = config;
