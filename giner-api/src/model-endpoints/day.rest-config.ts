import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Day} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Day,
  pattern: 'CrudRest',
  dataSource: 'db',
  basePath: '/days',
};
module.exports = config;
