import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Day,
  Week,
} from '../models';
import {DayRepository} from '../repositories';

export class DayWeekController {
  constructor(
    @repository(DayRepository)
    public dayRepository: DayRepository,
  ) { }

  @get('/days/{id}/week', {
    responses: {
      '200': {
        description: 'Week belonging to Day',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Week)},
          },
        },
      },
    },
  })
  async getWeek(
    @param.path.number('id') id: typeof Day.prototype.id,
  ): Promise<Week> {
    return this.dayRepository.week(id);
  }
}
