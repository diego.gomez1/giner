import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Course,
  Level,
} from '../models';
import {CourseRepository} from '../repositories';

export class CourseLevelController {
  constructor(
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
  ) { }

  @get('/courses/{id}/level', {
    responses: {
      '200': {
        description: 'Level belonging to Course',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Level)},
          },
        },
      },
    },
  })
  async getLevel(
    @param.path.number('id') id: typeof Course.prototype.id,
  ): Promise<Level> {
    return this.courseRepository.level(id);
  }
}
