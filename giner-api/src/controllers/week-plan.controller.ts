import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Week,
  Plan,
} from '../models';
import {WeekRepository} from '../repositories';

export class WeekPlanController {
  constructor(
    @repository(WeekRepository)
    public weekRepository: WeekRepository,
  ) { }

  @get('/weeks/{id}/plan', {
    responses: {
      '200': {
        description: 'Plan belonging to Week',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Plan)},
          },
        },
      },
    },
  })
  async getPlan(
    @param.path.number('id') id: typeof Week.prototype.id,
  ): Promise<Plan> {
    return this.weekRepository.plan(id);
  }
}
