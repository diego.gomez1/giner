import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Subject,
  Course,
} from '../models';
import {SubjectRepository} from '../repositories';

export class SubjectCourseController {
  constructor(
    @repository(SubjectRepository)
    public subjectRepository: SubjectRepository,
  ) { }

  @get('/subjects/{id}/course', {
    responses: {
      '200': {
        description: 'Course belonging to Subject',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Course)},
          },
        },
      },
    },
  })
  async getCourse(
    @param.path.number('id') id: typeof Subject.prototype.id,
  ): Promise<Course> {
    return this.subjectRepository.course(id);
  }
}
