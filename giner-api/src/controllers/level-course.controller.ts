import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Level,
  Course,
} from '../models';
import {LevelRepository} from '../repositories';

export class LevelCourseController {
  constructor(
    @repository(LevelRepository) protected levelRepository: LevelRepository,
  ) { }

  @get('/levels/{id}/courses', {
    responses: {
      '200': {
        description: 'Array of Level has many Course',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Course)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Course>,
  ): Promise<Course[]> {
    return this.levelRepository.courses(id).find(filter);
  }

  @post('/levels/{id}/courses', {
    responses: {
      '200': {
        description: 'Level model instance',
        content: {'application/json': {schema: getModelSchemaRef(Course)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Level.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Course, {
            title: 'NewCourseInLevel',
            exclude: ['id'],
            optional: ['levelId']
          }),
        },
      },
    }) course: Omit<Course, 'id'>,
  ): Promise<Course> {
    return this.levelRepository.courses(id).create(course);
  }

  @patch('/levels/{id}/courses', {
    responses: {
      '200': {
        description: 'Level.Course PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Course, {partial: true}),
        },
      },
    })
    course: Partial<Course>,
    @param.query.object('where', getWhereSchemaFor(Course)) where?: Where<Course>,
  ): Promise<Count> {
    return this.levelRepository.courses(id).patch(course, where);
  }

  @del('/levels/{id}/courses', {
    responses: {
      '200': {
        description: 'Level.Course DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Course)) where?: Where<Course>,
  ): Promise<Count> {
    return this.levelRepository.courses(id).delete(where);
  }
}
