import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Plan,
  Week,
} from '../models';
import {PlanRepository} from '../repositories';

export class PlanWeekController {
  constructor(
    @repository(PlanRepository) protected planRepository: PlanRepository,
  ) { }

  @get('/plans/{id}/weeks', {
    responses: {
      '200': {
        description: 'Array of Plan has many Week',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Week)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Week>,
  ): Promise<Week[]> {
    return this.planRepository.weeks(id).find(filter);
  }

  @post('/plans/{id}/weeks', {
    responses: {
      '200': {
        description: 'Plan model instance',
        content: {'application/json': {schema: getModelSchemaRef(Week)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Plan.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Week, {
            title: 'NewWeekInPlan',
            exclude: ['id'],
            optional: ['planId']
          }),
        },
      },
    }) week: Omit<Week, 'id'>,
  ): Promise<Week> {
    return this.planRepository.weeks(id).create(week);
  }

  @patch('/plans/{id}/weeks', {
    responses: {
      '200': {
        description: 'Plan.Week PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Week, {partial: true}),
        },
      },
    })
    week: Partial<Week>,
    @param.query.object('where', getWhereSchemaFor(Week)) where?: Where<Week>,
  ): Promise<Count> {
    return this.planRepository.weeks(id).patch(week, where);
  }

  @del('/plans/{id}/weeks', {
    responses: {
      '200': {
        description: 'Plan.Week DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Week)) where?: Where<Week>,
  ): Promise<Count> {
    return this.planRepository.weeks(id).delete(where);
  }
}
