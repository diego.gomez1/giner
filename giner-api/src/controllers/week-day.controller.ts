import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Week,
  Day,
} from '../models';
import {WeekRepository} from '../repositories';

export class WeekDayController {
  constructor(
    @repository(WeekRepository) protected weekRepository: WeekRepository,
  ) { }

  @get('/weeks/{id}/days', {
    responses: {
      '200': {
        description: 'Array of Week has many Day',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Day)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Day>,
  ): Promise<Day[]> {
    return this.weekRepository.days(id).find(filter);
  }

  @post('/weeks/{id}/days', {
    responses: {
      '200': {
        description: 'Week model instance',
        content: {'application/json': {schema: getModelSchemaRef(Day)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Week.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Day, {
            title: 'NewDayInWeek',
            exclude: ['id'],
            optional: ['weekId']
          }),
        },
      },
    }) day: Omit<Day, 'id'>,
  ): Promise<Day> {
    return this.weekRepository.days(id).create(day);
  }

  @patch('/weeks/{id}/days', {
    responses: {
      '200': {
        description: 'Week.Day PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Day, {partial: true}),
        },
      },
    })
    day: Partial<Day>,
    @param.query.object('where', getWhereSchemaFor(Day)) where?: Where<Day>,
  ): Promise<Count> {
    return this.weekRepository.days(id).patch(day, where);
  }

  @del('/weeks/{id}/days', {
    responses: {
      '200': {
        description: 'Week.Day DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Day)) where?: Where<Day>,
  ): Promise<Count> {
    return this.weekRepository.days(id).delete(where);
  }
}
