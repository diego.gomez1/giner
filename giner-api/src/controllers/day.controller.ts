import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Day} from '../models';
import {DayRepository} from '../repositories';

export class DayController {
  constructor(
    @repository(DayRepository)
    public dayRepository : DayRepository,
  ) {}

  @post('/days')
  @response(200, {
    description: 'Day model instance',
    content: {'application/json': {schema: getModelSchemaRef(Day)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Day, {
            title: 'NewDay',
            exclude: ['id'],
          }),
        },
      },
    })
    day: Omit<Day, 'id'>,
  ): Promise<Day> {
    return this.dayRepository.create(day);
  }

  @get('/days/count')
  @response(200, {
    description: 'Day model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Day) where?: Where<Day>,
  ): Promise<Count> {
    return this.dayRepository.count(where);
  }

  @get('/days')
  @response(200, {
    description: 'Array of Day model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Day, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Day) filter?: Filter<Day>,
  ): Promise<Day[]> {
    return this.dayRepository.find(filter);
  }

  @patch('/days')
  @response(200, {
    description: 'Day PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Day, {partial: true}),
        },
      },
    })
    day: Day,
    @param.where(Day) where?: Where<Day>,
  ): Promise<Count> {
    return this.dayRepository.updateAll(day, where);
  }

  @get('/days/{id}')
  @response(200, {
    description: 'Day model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Day, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Day, {exclude: 'where'}) filter?: FilterExcludingWhere<Day>
  ): Promise<Day> {
    return this.dayRepository.findById(id, filter);
  }

  @patch('/days/{id}')
  @response(204, {
    description: 'Day PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Day, {partial: true}),
        },
      },
    })
    day: Day,
  ): Promise<void> {
    await this.dayRepository.updateById(id, day);
  }

  @put('/days/{id}')
  @response(204, {
    description: 'Day PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() day: Day,
  ): Promise<void> {
    await this.dayRepository.replaceById(id, day);
  }

  @del('/days/{id}')
  @response(204, {
    description: 'Day DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.dayRepository.deleteById(id);
  }
}
