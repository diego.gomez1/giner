import {Entity, hasMany, model, property} from '@loopback/repository';
import {Week} from './week.model';

@model()
export class Plan extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  caseDocumentId?: string;

  @property({
    type: 'string',
  })
  caseDocumentRootUri?: string;

  @hasMany(() => Week)
  weeks: Week[];

  constructor(data?: Partial<Plan>) {
    super(data);
  }
}

export interface PlanRelations {
  // describe navigational properties here
}

export type PlanWithRelations = Plan & PlanRelations;
