import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Course} from './course.model';

@model()
export class Subject extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  short?: string;

  @belongsTo(() => Course)
  courseId: number;

  @property({
    type: 'number',
  })
  learningUnitId?: number;

  constructor(data?: Partial<Subject>) {
    super(data);
  }
}

export interface SubjectRelations {
  // describe navigational properties here
}

export type SubjectWithRelations = Subject & SubjectRelations;
