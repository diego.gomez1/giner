import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Week} from './week.model';

@model()
export class Day extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'number',
  })
  order?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property.array(String)
  tags: string[];

  @property.array(String)
  caseRefs: string[];

  @belongsTo(() => Week)
  weekId: number;

  constructor(data?: Partial<Day>) {
    super(data);
  }
}

export interface DayRelations {
  // describe navigational properties here
}

export type DayWithRelations = Day & DayRelations;
