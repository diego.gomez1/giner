import {Entity, model, property, hasMany} from '@loopback/repository';
import {Course} from './course.model';

@model()
export class Level extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  short: string;

  @hasMany(() => Course)
  courses: Course[];

  constructor(data?: Partial<Level>) {
    super(data);
  }
}

export interface LevelRelations {
  // describe navigational properties here
}

export type LevelWithRelations = Level & LevelRelations;
