import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Plan} from './plan.model';
import {Day} from './day.model';

@model()
export class Week extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  title?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'number',
  })
  order?: number;

  @property({
    type: 'date',
  })
  startAt?: string;

  @belongsTo(() => Plan)
  planId: number;

  @hasMany(() => Day)
  days: Day[];

  constructor(data?: Partial<Week>) {
    super(data);
  }
}

export interface WeekRelations {
  // describe navigational properties here
}

export type WeekWithRelations = Week & WeekRelations;
