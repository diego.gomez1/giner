import {Entity, model, property} from '@loopback/repository';

@model()
export class LearningUnit extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  overview?: string;

  constructor(data?: Partial<LearningUnit>) {
    super(data);
  }
}

export interface LearningUnitRelations {
  // describe navigational properties here
}

export type LearningUnitWithRelations = LearningUnit & LearningUnitRelations;
