import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Course, CourseRelations, Level} from '../models';
import {LevelRepository} from './level.repository';

export class CourseRepository extends DefaultCrudRepository<
  Course,
  typeof Course.prototype.id,
  CourseRelations
> {

  public readonly level: BelongsToAccessor<Level, typeof Course.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('LevelRepository') protected levelRepositoryGetter: Getter<LevelRepository>,
  ) {
    super(Course, dataSource);
    this.level = this.createBelongsToAccessorFor('level', levelRepositoryGetter,);
    this.registerInclusionResolver('level', this.level.inclusionResolver);
  }
}
