import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Week, WeekRelations, Plan, Day} from '../models';
import {PlanRepository} from './plan.repository';
import {DayRepository} from './day.repository';

export class WeekRepository extends DefaultCrudRepository<
  Week,
  typeof Week.prototype.id,
  WeekRelations
> {

  public readonly plan: BelongsToAccessor<Plan, typeof Week.prototype.id>;

  public readonly days: HasManyRepositoryFactory<Day, typeof Week.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PlanRepository') protected planRepositoryGetter: Getter<PlanRepository>, @repository.getter('DayRepository') protected dayRepositoryGetter: Getter<DayRepository>,
  ) {
    super(Week, dataSource);
    this.days = this.createHasManyRepositoryFactoryFor('days', dayRepositoryGetter,);
    this.registerInclusionResolver('days', this.days.inclusionResolver);
    this.plan = this.createBelongsToAccessorFor('plan', planRepositoryGetter,);
    this.registerInclusionResolver('plan', this.plan.inclusionResolver);
  }
}
