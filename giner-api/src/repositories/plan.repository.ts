import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Plan, PlanRelations, Week} from '../models';
import {WeekRepository} from './week.repository';

export class PlanRepository extends DefaultCrudRepository<
  Plan,
  typeof Plan.prototype.id,
  PlanRelations
> {

  public readonly weeks: HasManyRepositoryFactory<Week, typeof Plan.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('WeekRepository') protected weekRepositoryGetter: Getter<WeekRepository>,
  ) {
    super(Plan, dataSource);
    this.weeks = this.createHasManyRepositoryFactoryFor('weeks', weekRepositoryGetter,);
    this.registerInclusionResolver('weeks', this.weeks.inclusionResolver);
  }
}
