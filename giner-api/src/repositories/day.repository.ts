import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Day, DayRelations, Week} from '../models';
import {WeekRepository} from './week.repository';

export class DayRepository extends DefaultCrudRepository<
  Day,
  typeof Day.prototype.id,
  DayRelations
> {

  public readonly week: BelongsToAccessor<Week, typeof Day.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('WeekRepository') protected weekRepositoryGetter: Getter<WeekRepository>,
  ) {
    super(Day, dataSource);
    this.week = this.createBelongsToAccessorFor('week', weekRepositoryGetter,);
    this.registerInclusionResolver('week', this.week.inclusionResolver);
  }
}
