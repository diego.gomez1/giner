import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {LearningUnit, LearningUnitRelations} from '../models';

export class LearningUnitRepository extends DefaultCrudRepository<
  LearningUnit,
  typeof LearningUnit.prototype.id,
  LearningUnitRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(LearningUnit, dataSource);
  }
}
