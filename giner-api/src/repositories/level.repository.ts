import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Level, LevelRelations, Course} from '../models';
import {CourseRepository} from './course.repository';

export class LevelRepository extends DefaultCrudRepository<
  Level,
  typeof Level.prototype.id,
  LevelRelations
> {

  public readonly courses: HasManyRepositoryFactory<Course, typeof Level.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('CourseRepository') protected courseRepositoryGetter: Getter<CourseRepository>,
  ) {
    super(Level, dataSource);
    this.courses = this.createHasManyRepositoryFactoryFor('courses', courseRepositoryGetter,);
    this.registerInclusionResolver('courses', this.courses.inclusionResolver);
  }
}
