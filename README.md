# Commands to mantain project

![Diagrama de sistemas](doc/assets/systems.drawio.svg)

## Migrate database

This is necesary everytime the model changes or is needed to create a new database.

```bash
$ docker-compose -f docker-compose.dev.yml exec api npm run migrate
```

Restart server

```bash
docker-compose -f docker-compose.dev.yml restart
```

Go back to `giner-client` folder to recreate sdk

```bash
giner-client$ npm run gen-sdk
```

## Backup Database

Use `Adminer Tool` to make the backup. Access to url https://localhost:8080

```bash
docker-compose -f docker-compose.dev.yml exec db pg_dumpall -c -U postgres > backups/dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
```
