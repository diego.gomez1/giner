#!/bin/bash

#lastBackup=$(ls ./backups/*.gz -1t | head -1)
lastBackup=$(ls ./backups/*.tar -1t | head -1)
echo $lastBackup

read -p "Seguro que quieres restaurar $lastBackup? [y/N]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # do dangerous stuff    
    echo "Cargando"
    # gunzip < $lastBackup | docker-compose -f docker-compose.dev.yml exec -T database psql -U giner_user -d giner_database
    # zcat $lastBackup | docker-compose -f docker-compose.dev.yml exec -T database pg_restore -C --clean --no-acl --no-owner -U giner_user -d giner_database
    # zcat $lastBackup | docker-compose -f docker-compose.dev.yml exec -T database psql -U giner_user -d giner_database
    # zcat $lastBackup | docker-compose -f docker-compose.dev.yml exec -T database pg_restore -Ft -C --verbose --clean --no-acl --no-owner -U giner_user -d giner_database
    docker-compose -f docker-compose.dev.yml exec -T database pg_restore -C --verbose --clean --no-acl --no-owner -U giner_user -d giner_database < $lastBackup
else
    echo "Salgo sin cambios."
fi
