--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE giner_database;




--
-- Drop roles
--

DROP ROLE giner_user;


--
-- Roles
--

CREATE ROLE giner_user;
ALTER ROLE giner_user WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md52afbe2409dc028ed53ea3318b7b69887';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1 (Debian 13.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: giner_user
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO giner_user;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: giner_user
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: giner_user
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: giner_user
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "giner_database" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1 (Debian 13.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: giner_database; Type: DATABASE; Schema: -; Owner: giner_user
--

CREATE DATABASE giner_database WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE giner_database OWNER TO giner_user;

\connect giner_database

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: day; Type: TABLE; Schema: public; Owner: giner_user
--

CREATE TABLE public.day (
    id integer NOT NULL,
    name text,
    "order" integer,
    description text,
    weekid integer,
    caserefs text
);


ALTER TABLE public.day OWNER TO giner_user;

--
-- Name: day_id_seq; Type: SEQUENCE; Schema: public; Owner: giner_user
--

CREATE SEQUENCE public.day_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.day_id_seq OWNER TO giner_user;

--
-- Name: day_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: giner_user
--

ALTER SEQUENCE public.day_id_seq OWNED BY public.day.id;


--
-- Name: plan; Type: TABLE; Schema: public; Owner: giner_user
--

CREATE TABLE public.plan (
    id integer NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE public.plan OWNER TO giner_user;

--
-- Name: plan_id_seq; Type: SEQUENCE; Schema: public; Owner: giner_user
--

CREATE SEQUENCE public.plan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plan_id_seq OWNER TO giner_user;

--
-- Name: plan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: giner_user
--

ALTER SEQUENCE public.plan_id_seq OWNED BY public.plan.id;


--
-- Name: week; Type: TABLE; Schema: public; Owner: giner_user
--

CREATE TABLE public.week (
    id integer NOT NULL,
    title text,
    description text,
    "order" integer,
    startat timestamp with time zone,
    planid integer
);


ALTER TABLE public.week OWNER TO giner_user;

--
-- Name: week_id_seq; Type: SEQUENCE; Schema: public; Owner: giner_user
--

CREATE SEQUENCE public.week_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.week_id_seq OWNER TO giner_user;

--
-- Name: week_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: giner_user
--

ALTER SEQUENCE public.week_id_seq OWNED BY public.week.id;


--
-- Name: day id; Type: DEFAULT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.day ALTER COLUMN id SET DEFAULT nextval('public.day_id_seq'::regclass);


--
-- Name: plan id; Type: DEFAULT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.plan ALTER COLUMN id SET DEFAULT nextval('public.plan_id_seq'::regclass);


--
-- Name: week id; Type: DEFAULT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.week ALTER COLUMN id SET DEFAULT nextval('public.week_id_seq'::regclass);


--
-- Data for Name: day; Type: TABLE DATA; Schema: public; Owner: giner_user
--

COPY public.day (id, name, "order", description, weekid, caserefs) FROM stdin;
\.


--
-- Data for Name: plan; Type: TABLE DATA; Schema: public; Owner: giner_user
--

COPY public.plan (id, name, description) FROM stdin;
2	TSDAM - Curso 2	<p>Planificación del 2º curso de TSDAM</p><p>Aquí va otra prueba</p>
4	TSDAW - Curso 2	<p>Planificación del 2º curso de <strong>TSDAW</strong></p><p>Algunos de los principios más relevantes de este curso son:</p><ul><li>Cuidado de las habilidades conseguidas.</li><li>Notificación de las ausencias.</li><li>Gamificación.</li></ul>
1	TSDAM - Curso 1	<p>Planificación del 1º curso de TSDAM</p><p>El primer curso es común con el ciclo TSDAW.</p>
3	TSDAW - Curso 1	<p>Planificación del 1º curso de <strong>TSDAW</strong></p><p>El 1º curso de TSDAW es común con TSDAM consultar el <a href="http://127.0.0.1:4200/plans/1/edit" rel="noopener noreferrer" target="_blank">detalle de este curso</a>.</p>
\.


--
-- Data for Name: week; Type: TABLE DATA; Schema: public; Owner: giner_user
--

COPY public.week (id, title, description, "order", startat, planid) FROM stdin;
\.


--
-- Name: day_id_seq; Type: SEQUENCE SET; Schema: public; Owner: giner_user
--

SELECT pg_catalog.setval('public.day_id_seq', 1, false);


--
-- Name: plan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: giner_user
--

SELECT pg_catalog.setval('public.plan_id_seq', 4, true);


--
-- Name: week_id_seq; Type: SEQUENCE SET; Schema: public; Owner: giner_user
--

SELECT pg_catalog.setval('public.week_id_seq', 1, false);


--
-- Name: day day_pkey; Type: CONSTRAINT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.day
    ADD CONSTRAINT day_pkey PRIMARY KEY (id);


--
-- Name: plan plan_pkey; Type: CONSTRAINT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.plan
    ADD CONSTRAINT plan_pkey PRIMARY KEY (id);


--
-- Name: week week_pkey; Type: CONSTRAINT; Schema: public; Owner: giner_user
--

ALTER TABLE ONLY public.week
    ADD CONSTRAINT week_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1 (Debian 13.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: giner_user
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO giner_user;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: giner_user
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

